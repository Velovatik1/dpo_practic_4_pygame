import pygame
import time
from Robot import Robot
import math
from pid import pid

pygame.init()
screen = pygame.display.set_mode((1000, 500))
done = False

t = 0
G = 9.81
dt = 0.2

vy = 0
sy0 = 0
sy = 0

robot1 = Robot(10)

trace = pid(robot1.height)

draw_flight = True

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    if draw_flight:
        for point in trace:

            sy = point

            screen.fill((0, 0, 0))

            pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(250, 500 - sy * 10, 10, 10))
            pygame.display.flip()
            t += dt
            time.sleep(dt)

            draw_flight = False

    pygame.draw.rect(surface=screen, color=(0, 255, 0), rect=pygame.Rect(250, 500 - sy * 10, 10, 10))
    pygame.display.flip()

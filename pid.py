def pid(setpoint_value):
    t_end = 500
    n_steps = 50
    dt = t_end / n_steps

    # Первое значение измерений
    measurements = [0]
    errors = [setpoint_value - measurements[0]]  # Отклонение от нужного значения. На каждой итерации учитывается
    # предыдущая ошибка

    # Set the settings of the PID controller (according to the values used in the example table)
    Kp = 0.5
    Ki = 0.1
    Kd = 0.2

    integral_values = [0]
    proportional_values = [0]
    derivative_values = [0]
    pid_values = [0]
    integral = 0
    previous_error = 0  # Variable to store the previous error value

    for i in range(1, n_steps):
        e = errors[-1]  # Предыдущая ошибка

        integral += e * dt  # Текущее значение интегральной составляющей. Ошибка копится
        proportional = Kp * e  # Пропорциональное регулирование основывается на текущем отклонении (error)
        integral_contribution = Ki * integral  # Исправляет прошлые накопившиеся ошибки
        derivative = Kd * (e - previous_error) / dt  # Производная ошибки по времени.
        #  Следит, чтобы систему не раскачивали сильно

        pid = proportional + integral_contribution + derivative  # Суммарное воздействие

        # Add the PID output to our current value
        measurements.append(measurements[-1] + pid)

        # Calculate the error for the next step
        error = setpoint_value - measurements[-1]  # Ошибка на текущем шаге
        errors.append(error)

        integral_values.append(integral_contribution)  # Не нужны -----
        proportional_values.append(proportional)
        derivative_values.append(derivative)
        pid_values.append(pid)  # Не нужны --------!

        previous_error = e

    return measurements
